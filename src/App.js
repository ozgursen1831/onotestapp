import React from 'react';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {createBrowserHistory} from 'history';
import './App.css';
import MyChart from './MyChart';
import Signin from './signin';


const history=new createBrowserHistory();
const App = () => (
  <Router>
    <Switch>
      <Route path="/" exact component={Signin} />
      <Route path="/chart" exact component={MyChart} />
    </Switch>
  </Router>
)

export default App;
