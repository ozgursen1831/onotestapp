import React, { useEffect, useState } from "react";
import axios from "axios";
import { Container, Grid, Paper } from "@mui/material";
import Chart from "react-apexcharts";
import QuickChart from 'quickchart-js';

function MyChart() {
  const [url, setUrl] = useState("https://quickchart.io/chart?c=%7Btype%3A%27bar%27%2Cdata%3A%7Blabels%3A%5B%27Hello+world%27%2C%27Foo+bar%27%5D%2Cdatasets%3A%5B%7Blabel%3A%27Foo%27%2Cdata%3A%5B1%2C2%5D%7D%5D%7D%7D&w=500&h=300&bkg=transparent&f=png");

  const [chartData, setChartData] = useState({
    options: {
      chart: {
        id: "basic-bar"
      },
      xaxis: {
        categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
      }
    },
    series: [
      {
        name: "series-1",
        data: [30, 40, 45, 50, 49, 60, 70, 120, 89]
      }
    ]
  });

  useEffect(() => {
    const c = { type: 'bar', data: { labels: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999], datasets: [{ label: 'Users', data: [30, 40, 45, 50, 49, 60, 70, 120, 89] }] } };
    console.log(JSON.stringify(c));
    const url = `https://quickchart.io/chart?c=${JSON.stringify(c)}`;
    setUrl(url);
    console.log("UseEffect is working!!!");
  });


  return (
    <Container fixed>
      <Paper >
        <Grid container spacing={2} maxWidth="sm"   >
          <Grid item xs={12}  >
            <Chart options={chartData.options}
              series={chartData.series}
              type="bar"
              width="1000"
            />
          </Grid>
        </Grid></Paper>
      <img src={url} />
    </Container>


  )

};
export default MyChart;

