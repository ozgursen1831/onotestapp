import React, { useState } from 'react';
import { Grid, Container,TextField,Button } from '@mui/material';
import { Redirect } from "react-router-dom";

function Signin() {
    const [mail, setMail] = useState(null);
    const [pass, setPass] = useState(null);
    const [userRedirect , setUserRedirect] = useState(false);
    const [logedUser, setLogedUser]=useState();

    const users = [
        {
            _İd: "0001",
            email: "onosoftware@gmail.com",
            password: "123456"
        },
        {
            _id: "0002",
            email: "ozgursen@gmail.com",
            password: "123456"
        }
    ];

    const signin = () => {
       
        let validUser = users.filter(item => {
            return item.email === mail && item.password === pass
        });
        console.log(validUser);
        if(validUser.length){
            setUserRedirect('/chart');
            setLogedUser(validUser[0]);
        } 
    };

    if (userRedirect ){
       return <Redirect to={userRedirect} />
    }


    return (
        <Container>
            <Grid container spacing={2} maxWidth="sm">
                <Grid item xs={12}>
                    <label>Email</label><br />
                    <TextField size='small' name="user-email" onChange={(event) => setMail(event.target.value)} />
                </Grid>

                <Grid item xs={12}>
                    <label>Password</label><br />
                    <TextField  size='small' name="user-password" onChange={(event) => setPass(event.target.value)} />
                </Grid>

                <Grid item xs={12}>
                    <Button variant="contained" color="primary" size="small" onClick={() => signin()}>Signin</Button>
                </Grid>
            </Grid>
        </Container>
    );
}

export default Signin;
